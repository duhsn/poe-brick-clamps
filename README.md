Author: Dustin Schroll May 25 2019 Flatwater Networks LLC

These designs are offered free to use at your own risk.

Import STL files into a program Like Slicer to create your gcode for your
3D printer.

You can purchase the clamps and see more information and photos of them at 
https://www.flatwaterwireless.com/wisp-products

Hopefully these designs save lots and lots of fuel and time from saved service
calls to simply re-plug in cables for customers.